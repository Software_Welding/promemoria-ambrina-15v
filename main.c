#include <msp430.h>                      // Generic MSP430 Device Include
#include "driverlib.h"                   // MSPWare Driver Library
#include "captivate.h"                   // CapTIvate Touch Software Library
#include "CAPT_App.h"                    // CapTIvate Application Code
#include "CAPT_BSP.h"                    // CapTIvate EVM Board Support Package

#define Fiammetta
#define NEGOZIATORE_ESTERNO

#define MINIMUM_SHORT_TOUCH_TIME                10
#define MAXIMUM_SHORT_TOUCH_TIME			   400

#define MANUAL_RAMP_DELAY_TIME               10000		//Cycle
#define AUTOMATIC_RAMP_DELAY_TIME             5000      //Cycle

#ifndef Fiammetta
#define MIN_PWM				   					10
#define MAX_PWM								   180
#else
#define MIN_PWM				   					 5
//#define MAX_PWM								    50
#endif
//#define STARTUP_THREESHOLD                     800
//#define ATTACHED                               P2IN & GPIO_PIN5
#define FRAM_TEST_START 0x1800

/*
 * ADC Lettura batteria per inizio VRef = 3.3v
 */
#define BATTERY_IN_DISCHARGE                   575  // 10.1 vbatt
#define BATTERY_FULL						   688  //693 12.1v
#define THREESHOLD_OUT_LOW_BATTERY              50  // 10.9 vbatt
#define BATTERIA_SI_STA_SCARICANDO ((ADC_Result < BATTERY_IN_DISCHARGE) || (first_enter_low_battery == true))
#define BATTERIA_CARICA ADC_Result > BATTERY_FULL
#define SWEEP_TIME_ON_OFF 10000

#define TIMER_PWM_OUT TA0CCR1
/* Configurazioni Fiammetta originali
 *
 * MIN_PWM 5
 * MAX_PWM 50
 *
 * STARTUP_THREESHOLD 800
 * ATTACHED P2IN & GPIO_PIN5
 */
#define MAX_PWM 255
#define STARTUP_THREESHOLD 560
#define ATTACHED P2IN & GPIO_PIN3

unsigned char count = 0;
unsigned long *FRAM_write_ptr;
unsigned long *FRAM_Read_ptr;
unsigned long data;

#define FRAM_TEST_START 0x1800
#define _5_Sec 5000
#define _1_Sec 1000
#define _15_Min 900000
#define _10_Min 600000
#define _1_Min 60000
#define VALORE_BAT_CARICA 220
#define VALORE_BATTERIA_SCARICA_LED_ON 190
#define VALORE_BATTERIA_SCARICA_LED_OFF 205

#define BATTERIA_COMPLETAMENTE_SCARICA (ADC_Result < BATTERY_IN_DISCHARGE)
#define SOGLIA_BATTERIA_CARICA (ADC_Result > (BATTERY_IN_DISCHARGE + THREESHOLD_OUT_LOW_BATTERY))

uint16_t soglia_adc_off = VALORE_BATTERIA_SCARICA_LED_OFF;
long mSecTouch = 0;
char button_old = 0;
char is_to_be_negotiated = 0;
bool first_enter_low_battery = false;
bool batteria_completamente_scarica = false;
bool batteria_completamente_scarica_first_enter = false;
uint8_t Array_Discharge_Timer_Index = 0;

uint32_t Array_Discharge_Timer_Value[15] =
{
 1000,
 300000,
 240000, // 15 m
 180000,
 120000, // 20 m
 60000,
 60000,
 60000,
 60000,
 60000, // 25 m
 60000,
 60000,
 60000,
 60000,
 60000,
};

char DimmingLevel = 1;
int TargetLevel = 0;
int LedStatus=0;
uint32_t timer_discharge = 0;

char FallingEdge = 1;
char RisingEdge = 0;

unsigned long i = 0;
unsigned long ADC_Result;


typedef enum edge_enum {
	None,
	Rise,
	Fall
} edge_typdef;
edge_typdef Edge = None;

typedef enum power_enum {
	Off,
	On
} power_typdef;
power_typdef Power = Off;

typedef enum dimming_direction_enum {
	Down,
	Up
} dimming_direction_typdef;
dimming_direction_typdef DimmingDirection = Up;
uint16_t mSecADC = 0;
uint16_t mSecNegoziatore = 0;


//******************************************************************************
// General I2C State Machine ***************************************************
//******************************************************************************

typedef enum I2C_ModeEnum{
	IDLE_MODE,
	NACK_MODE,
	TX_REG_ADDRESS_MODE,
	RX_REG_ADDRESS_MODE,
	TX_DATA_MODE,
	RX_DATA_MODE,
	SWITCH_TO_RX_MODE,
	SWITHC_TO_TX_MODE,
	TIMEOUT_MODE
} I2C_Mode;

I2C_Mode MasterMode = IDLE_MODE;

uint8_t TransmitRegAddr = 0;

#define MAX_BUFFER_SIZE     20

uint8_t ReceiveBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t RXByteCtr = 0;
uint8_t ReceiveIndex = 0;
uint8_t TransmitBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t TXByteCtr = 0;
uint8_t TransmitIndex = 0;
uint8_t first_run = 0;
I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count);
I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count);
void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count);
void change_pdo(uint8_t pdo_number);
void Update_ADC_Value (void);
void FRAMRead (void);
void FRAMWrite (void);
void initI2C();
void init_adc1 (void);
void FadeOn (void);
void FadeOff (void);
void Delay_With_Interrupt (uint32_t delay);

void set_pdo(uint16_t volt, uint16_t amp, uint8_t pdo_number);

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;
	BSP_configureMCU();
	initI2C();
	init_adc1();


	if(!(ATTACHED))
	{
		//set_pdo(9, 2, 2);
		//set_pdo(9, 2, 3);
		set_pdo(15,2,2);
		set_pdo(15,2,3);
		change_pdo(3);
	}

	P2OUT |=  BIT5;                          	// Configure P2.5 as pulled-up
	P2REN |=  BIT5;                          	// P2.5 pull-up register enable
	if(P2IN & GPIO_PIN5) P2IES |=  BIT5;        // P2.5 Hi/Low edge
	else P2IES &= ~BIT5;                        // P2.5 Low/Hi edge
	P2IE  |=  BIT5;                           	// P2.5 interrupt enabled
	P2IFG &= ~BIT5;                         	// P2.5 IFG cleared


    P1DIR |= 1;
	P1SEL1 = 2;                    // P1.1 options select

	PM5CTL0 &= ~LOCKLPM5;

	TA0CCR0 = 200 - 1;         		            // PWM Period
	TA0CCTL1 = OUTMOD_7;                      	// CCR1 reset/set
	TA0CCR1 = 0;                            	// CCR1 PWM duty cycle
	TA0CTL = TASSEL__SMCLK | MC__UP | TACLR;  	// SMCLK, up mode, clear TAR
	TA0CCR1 = 0;

	// Timer1_A3 setup
	TA1CCTL0 = CCIE;                              // TACCR0 interrupt enabled
	TA1CCR0 = 33-1;
	TA1CTL = TASSEL_1 | MC_1;                     // ACLK, up mode

	__bis_SR_register(GIE);
	PMMCTL2 |= BIT0;
	PMMCTL2  &= ~ BIT4;
	PMMCTL2  |= BIT5;


    _delay_cycles(50000);
	ADCCTL0 |= ADCENC | ADCSC;
    _delay_cycles(50000);
	ADCCTL0 |= ADCENC | ADCSC;
    _delay_cycles(50000);
	ADCCTL0 |= ADCENC | ADCSC;
    _delay_cycles(50000);

	FRAMRead();

	if (Power == On)
	{
		Edge = Rise;
		TargetLevel = DimmingLevel;
	}
	CAPT_appStart();

    if ((first_run == 0x00) || (first_run == 0xFF))
    {
    	first_run = 1;
    	batteria_completamente_scarica = false;
    	batteria_completamente_scarica_first_enter = false;
    }
	FRAMWrite();
	while(1)
    {

/*
		if(!(ATTACHED) && (mSecNegoziatore >= _1_Sec))
		{
			set_pdo(15,2,2);
			set_pdo(15,2,3);
			change_pdo(3);
			mSecNegoziatore = 0;
		}*/

	    Update_ADC_Value();

	    if (BATTERIA_COMPLETAMENTE_SCARICA)
	    {
	    	first_run = 2;
	    	batteria_completamente_scarica = true;
	    	if (batteria_completamente_scarica_first_enter == false)
	    	{
	    		Power = Off;
	    		TargetLevel = 0;
	    		Edge = Fall;
	    		batteria_completamente_scarica_first_enter = true;
	    	}
	    }

	    if ((SOGLIA_BATTERIA_CARICA) || (!(ATTACHED)))
	    {
	    	first_run = 1;
	    	batteria_completamente_scarica = false;
	    	batteria_completamente_scarica_first_enter = false;
	    }

	    /*
		if(is_to_be_negotiated == 1)
		{
			if(!(ATTACHED))		//if is low
			{
				//set_pdo(9, 2, 2);
				//set_pdo(9, 2, 3);
				set_pdo(15,2,2);
				set_pdo(15,2,3);
				change_pdo(3);
				is_to_be_negotiated = 0;
			}
		}
/*
		if (first_run == 1)
		{
			for (i = 0; i < MAX_PWM;i ++)
			{
				i = MANUAL_RAMP_DELAY_TIME;
				while(--i > 1) __no_operation();
				TA0CCR1 = i;
			}

			i = MANUAL_RAMP_DELAY_TIME;
			while(--i > 1) __no_operation();

			for (i = MAX_PWM; i > 0; i --)
			{
				i = MANUAL_RAMP_DELAY_TIME;
				while(--i > 1) __no_operation();
				TA0CCR1 = i;
			}

			i = MANUAL_RAMP_DELAY_TIME;
			while(--i > 1) __no_operation();
		}
*/

		/*
		 * TOUCH
		 */

		// touch toccato
		// first run identifica lo stato della batteria
		if (first_run == 1)
			{
			if(CAPT_appHandler() == true)
			{
				LED2_ON;
				if(button_old == 0)
				{
					RisingEdge = 1;
					FallingEdge = 0;
					mSecTouch  = 0;
				}
				button_old = 1;
			}

			// touch rilasciato
			else
			{
				LED2_OFF;
				if(button_old == 1)
				{
					RisingEdge = 0;
					FallingEdge = 1;

					if(mSecTouch > MAXIMUM_SHORT_TOUCH_TIME)
					{
						if(DimmingDirection == Up)
						{
							DimmingDirection = Down;
						}
						else
						{
							DimmingDirection = Up;
						}

						FRAMWrite();
						CAPT_appStart();
					}
				}
				button_old = 0;
			}


			/*
			 * GESTIONE TOCCHI
*/
			// tocco breve -> on / off
			if((FallingEdge == 1) && (mSecTouch > MINIMUM_SHORT_TOUCH_TIME) && (mSecTouch < MAXIMUM_SHORT_TOUCH_TIME))
			{
				if(Power == Off)
				{
					Power = On;
					TargetLevel = DimmingLevel;
					Edge = Rise;
					soglia_adc_off = VALORE_BATTERIA_SCARICA_LED_ON;
				}
				else
				{
					Power = Off;
					TargetLevel = 0;
					Edge = Fall;
					soglia_adc_off = VALORE_BATTERIA_SCARICA_LED_OFF;
				}

				mSecTouch = MAXIMUM_SHORT_TOUCH_TIME + 1;
			}

			// tocco lungo -> dimmerazione
			if((Power == On)&&(Edge == None))
			{
				if((RisingEdge)&&(mSecTouch > MAXIMUM_SHORT_TOUCH_TIME))
				{
					i = MANUAL_RAMP_DELAY_TIME;
					while(--i > 1) __no_operation();

					if(DimmingDirection == Up)
					{
						if((DimmingLevel < MAX_PWM)&&(RisingEdge == 1)) DimmingLevel ++;			//Controllo se il tasto � ancora premuto
					}
					else
					{
						if((DimmingLevel > MIN_PWM)&&(RisingEdge == 1)) DimmingLevel --;			//Controllo se il tasto � ancora premuto
					}

					TA0CCR1 = DimmingLevel;                            	// CCR1 PWM duty cycle
				}
			}
		}


		// Accensione spegnimento: gestione pwm (first run)
		if((Edge == Fall) || (Edge == Rise))
		{
			i = AUTOMATIC_RAMP_DELAY_TIME;
			while(--i > 1) __no_operation();

			if(Edge == Fall)
			{
				if(TA0CCR1 > TargetLevel) TA0CCR1 --;
			}
			else if(Edge == Rise)
			{
				if(TA0CCR1 < TargetLevel) TA0CCR1 ++;
			}

			if(TA0CCR1 == TargetLevel)
			{
				Edge = None;
				FRAMWrite();
				CAPT_appStart();
			}
		}
	} // End background loop
} // End main()

void FadeOn (void)
{
        int i = 0;
        for (i = 0; i < DimmingLevel; i++)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
}
void FadeOff (void)
{
        int i = 0;
        for (i = DimmingLevel; i >= 0; i--)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
}
void Delay_With_Interrupt (uint32_t delay)
{
    int i = delay;
    while(--i > 1) __no_operation();
}
I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count)
{
	/* Initialize state machine */
	MasterMode = TX_REG_ADDRESS_MODE;
	TransmitRegAddr = reg_addr;
	RXByteCtr = count;
	TXByteCtr = 0;
	ReceiveIndex = 0;
	TransmitIndex = 0;

	/* Initialize slave address and interrupts */
	UCB0I2CSA = dev_addr;
	UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
	UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
	UCB0IE |= UCTXIE;                        // Enable TX interrupt

	UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
	__bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts
//	__bis_SR_register(GIE);              // Enter LPM0 w/ interrupts

	return MasterMode;

}


I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count)
{
	/* Initialize state machine */
	MasterMode = TX_REG_ADDRESS_MODE;
	TransmitRegAddr = reg_addr;

	//Copy register data to TransmitBuffer
	CopyArray(reg_data, TransmitBuffer, count);

	TXByteCtr = count;
	RXByteCtr = 0;
	ReceiveIndex = 0;
	TransmitIndex = 0;

	/* Initialize slave address and interrupts */
	UCB0I2CSA = dev_addr;
	UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
	UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
	UCB0IE |= UCTXIE;                        // Enable TX interrupt

	UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
	__bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts
//	__bis_SR_register(GIE);              // Enter LPM0 w/ interrupts

	return MasterMode;
}

void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count)
{
	uint8_t copyIndex = 0;
	for (copyIndex = 0; copyIndex < count; copyIndex++)
	{
		dest[copyIndex] = source[copyIndex];
	}
}

void initI2C()
{
    UCB0CTLW0 = UCSWRST;                     								// Enable SW reset
    UCB0CTLW0 |= UCMODE_3 | UCMST | UCSSEL__SMCLK | UCSYNC; 				// I2C master mode, SMCLK
    UCB0BRW = 20;                            								// fSCL = SMCLK/20 = ~100kHz
    UCB0I2CSA = 0x28;                   									// Slave Address
    UCB0CTLW0 &= ~UCSWRST;                    								// Clear SW reset, resume operation
    UCB0IE |= UCNACKIE;
}

void set_pdo(uint16_t volt, uint16_t amp, uint8_t pdo_number)
{
	uint8_t buffer_tx[5] = {0};
	volatile uint32_t pdo_reg = 0;
	pdo_reg = (volt * 20);
	pdo_reg = pdo_reg << 10;
	pdo_reg += (amp * 100);

	buffer_tx[0] = pdo_reg & 0xFF;
	buffer_tx[1] = (pdo_reg & 0xFF00) >> 8;
	buffer_tx[2] = (pdo_reg & 0xFF0000) >> 16;
	buffer_tx[3] = (pdo_reg & 0xFF000000) >> 24;

	if(pdo_number == 2) I2C_Master_WriteReg(0x28, 0x89, buffer_tx, 4);							//Address PDO2
	if(pdo_number == 3) I2C_Master_WriteReg(0x28, 0x8D, buffer_tx, 4);							//Address PDO3
}

void change_pdo(uint8_t pdo_number)
{
	uint8_t buffer_tx[2] = {0};

	buffer_tx[0] = pdo_number;
	I2C_Master_WriteReg(0x28, 0x70, buffer_tx, 1);					//PDO select

	buffer_tx[0] = 0x0D;
	I2C_Master_WriteReg(0x28, 0x51, buffer_tx, 1);					//Soft Reset

	buffer_tx[0] = 0x26;
	I2C_Master_WriteReg(0x28, 0x1A, buffer_tx, 1);					//Send Command
}

void init_adc1 (void)
{

SYSCFG2 |= ADCPCTL4;

ADCCTL0 |= ADCSHT_2 | ADCON;                             // ADCON, S&H=16 ADC clks
ADCCTL1 |= ADCSHP;                                       // ADCCLK = MODOSC; sampling timer
ADCCTL2 |= ADCRES;                                       // 10-bit conversion results
ADCMCTL0 |= ADCINCH_4;                                   // A1 ADC input select; Vref=AVCC
ADCIE |= ADCIE0;// Enable ADC conv complete interrupt
//ADCSREF = 0;

}

void FRAMWrite (void)
{

    FRAM_write_ptr = (unsigned long *)FRAM_TEST_START;
    SYSCFG0 = FRWPPW | PFWP;

    *FRAM_write_ptr = Power;
    *FRAM_write_ptr++;
    *FRAM_write_ptr = DimmingLevel;
    *FRAM_write_ptr++;
    *FRAM_write_ptr = DimmingDirection;
    *FRAM_write_ptr++;
    *FRAM_write_ptr = first_run;

    SYSCFG0 = FRWPPW | PFWP | DFWP;
}

void FRAMRead (void)
{

    FRAM_Read_ptr = (unsigned long *)FRAM_TEST_START;
    SYSCFG0 = FRWPPW | PFWP;

    Power =  *FRAM_Read_ptr;
    *FRAM_Read_ptr++;
    DimmingLevel = *FRAM_Read_ptr;
    *FRAM_Read_ptr++;
    DimmingDirection = *FRAM_Read_ptr;
    *FRAM_Read_ptr++;
    first_run = *FRAM_Read_ptr;

    SYSCFG0 = FRWPPW | PFWP | DFWP;
}
void Batteria_In_Scarica (void)
{
    if (BATTERIA_SI_STA_SCARICANDO)
    {
        first_enter_low_battery = true;
        if (Array_Discharge_Timer_Index == 0)
        {
            //Lamp_Set_PWM();
        }
        if (Array_Discharge_Timer_Index == 15)
        {
            TIMER_PWM_OUT = MIN_PWM;
            if (timer_discharge >= _15_Min)
            {
                TIMER_PWM_OUT = 255;
                //BATTERIA COMLETAMENTE SCARICA
                FRAMWrite();
            }
            return;
        }
        if (timer_discharge >= Array_Discharge_Timer_Value[Array_Discharge_Timer_Index])
        {
            timer_discharge = 0;
            Array_Discharge_Timer_Index ++;
            FadeOff();
            __delay_cycles(16000000);
            FadeOn();
        }
    }
    else
    {
        Array_Discharge_Timer_Index = 0;
        //Lamp_Set_PWM();
    }
}

void Update_ADC_Value (void)
{
    if (mSecADC >= _1_Sec) {
        ADCCTL0 |= ADCENC | ADCSC;
        mSecADC = 0;
    }

    if (mSecNegoziatore <= _1_Sec)
    {
    	mSecNegoziatore ++;
    }
}
//******************************************************************************
// I2C Interrupt ***************************************************************
//******************************************************************************
#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
	//Must read from UCB0RXBUF
	uint8_t rx_val = 0;
	switch(__even_in_range(UCB0IV, USCI_I2C_UCBIT9IFG))
	{
	case USCI_NONE:          break;         // Vector 0: No interrupts
	case USCI_I2C_UCALIFG:   break;         // Vector 2: ALIFG
	case USCI_I2C_UCNACKIFG:                // Vector 4: NACKIFG
		break;
	case USCI_I2C_UCSTTIFG:  break;         // Vector 6: STTIFG
	case USCI_I2C_UCSTPIFG:  break;         // Vector 8: STPIFG
	case USCI_I2C_UCRXIFG3:  break;         // Vector 10: RXIFG3
	case USCI_I2C_UCTXIFG3:  break;         // Vector 12: TXIFG3
	case USCI_I2C_UCRXIFG2:  break;         // Vector 14: RXIFG2
	case USCI_I2C_UCTXIFG2:  break;         // Vector 16: TXIFG2
	case USCI_I2C_UCRXIFG1:  break;         // Vector 18: RXIFG1
	case USCI_I2C_UCTXIFG1:  break;         // Vector 20: TXIFG1
	case USCI_I2C_UCRXIFG0:                 // Vector 22: RXIFG0
		rx_val = UCB0RXBUF;
		if (RXByteCtr)
		{
			ReceiveBuffer[ReceiveIndex++] = rx_val;
			RXByteCtr--;
		}

		if (RXByteCtr == 1)
		{
			UCB0CTLW0 |= UCTXSTP;
		}
		else if (RXByteCtr == 0)
		{
			UCB0IE &= ~UCRXIE;
			MasterMode = IDLE_MODE;
			__bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
		}
		break;
	case USCI_I2C_UCTXIFG0:                 // Vector 24: TXIFG0
		switch (MasterMode)
		{
		case TX_REG_ADDRESS_MODE:
			UCB0TXBUF = TransmitRegAddr;
			if (RXByteCtr)
				MasterMode = SWITCH_TO_RX_MODE;   // Need to start receiving now
				else
					MasterMode = TX_DATA_MODE;        // Continue to transmision with the data in Transmit Buffer
			break;

		case SWITCH_TO_RX_MODE:
			UCB0IE |= UCRXIE;              // Enable RX interrupt
			UCB0IE &= ~UCTXIE;             // Disable TX interrupt
			UCB0CTLW0 &= ~UCTR;            // Switch to receiver
			MasterMode = RX_DATA_MODE;    // State state is to receive data
			UCB0CTLW0 |= UCTXSTT;          // Send repeated start
			if (RXByteCtr == 1)
			{
				//Must send stop since this is the N-1 byte
				while((UCB0CTLW0 & UCTXSTT));
				UCB0CTLW0 |= UCTXSTP;      // Send stop condition
			}
			break;

		case TX_DATA_MODE:
			if (TXByteCtr)
			{
				UCB0TXBUF = TransmitBuffer[TransmitIndex++];
				TXByteCtr--;
			}
			else
			{
				//Done with transmission
				UCB0CTLW0 |= UCTXSTP;     // Send stop condition
				MasterMode = IDLE_MODE;
				UCB0IE &= ~UCTXIE;                       // disable TX interrupt
				__bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
			}
			break;

		default:
			__no_operation();
			break;
		}
		break;
		default: break;
	}
}


//******************************************************************************
// PORT2 Interrupt *************************************************************
//******************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    P2IFG &= ~BIT5;                        		// Clear P2.5 IFG
	if(P2IN & GPIO_PIN5)						//if is high
	{
		P2IES |=  BIT5;        					// P2.5 Hi/Low edge
	}
	else										//if is low
	{
		P2IES &= ~BIT5;                        // P2.5 Low/Hi edge
		is_to_be_negotiated = 1;
	}
    CAPT_appStart();
}


//******************************************************************************
// TIMER1 Interrupt ***************************************************************
//******************************************************************************
#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
{
	//ms
//	LED2_POUT ^= LED2_PIN;
	if(RisingEdge)
	{
		if(mSecTouch < MAXIMUM_SHORT_TOUCH_TIME + 1) mSecTouch ++;
	}

    mSecADC++;
    if (BATTERIA_SI_STA_SCARICANDO)
    {
    	if (timer_discharge < _15_Min)
        {
    		timer_discharge ++;
        }
    }

}

//******************************************************************************
// ADC1  Interrupt ***************************************************************
//******************************************************************************

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC_VECTOR
__interrupt void ADC_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC_VECTOR))) ADC_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(ADCIV,ADCIV_ADCIFG))
    {
        case ADCIV_NONE:
            break;
        case ADCIV_ADCOVIFG:
            break;
        case ADCIV_ADCTOVIFG:
            break;
        case ADCIV_ADCHIIFG:
            break;
        case ADCIV_ADCLOIFG:
            break;
        case ADCIV_ADCINIFG:
            break;
        case ADCIV_ADCIFG:
            ADC_Result = ADCMEM0;
            break;
        default:
            break;
    }
}
